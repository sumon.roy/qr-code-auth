import "./qr.styles.css";
import LoaderSvg from "../loader/loader.svg"

export default function QRCard(props) {
  const { svgState } = props;

  return (
    <div class="qrCard">
      <h1>Qr Card</h1>
      {
        svgState.value != null ? 
        <>
        <div dangerouslySetInnerHTML={svgState.value} >
        </div> 
          <span>
            scan this qr code on your mobile app to get authenticated
          </span>
        </>
        : 
        <>
        <LoaderSvg />
        <p>
          Your QR code will be appear here
        </p>
        </>
      }
    </div>
  );
}
