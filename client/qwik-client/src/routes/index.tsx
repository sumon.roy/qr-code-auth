import { component$, useTask$ } from "@builder.io/qwik";
import type { DocumentHead } from "@builder.io/qwik-city";
// import { Link } from '@builder.io/qwik-city';
import { useStore, useSignal } from "@builder.io/qwik";
import { LoginForm } from "../components/form/login.form";
import QRCard from "~/components/card/qr.card";
import "./index.styles.css";

export default component$(() => {
  const credStore = useStore({
    email: "",
    password: "",
  });

  const svgState = useSignal();

  useTask$(({ track }) => {
    track(credStore);
  });

  return (
    <div class="content">
      <p>
        Welcome to QR-Auther's Qwik App ⚡️
      </p>

      <div class="content-body">
        <LoginForm credStore={credStore} svgState={svgState}/>
        <QRCard svgState={svgState}/>
      </div>

      {/* <Link class="mindblow" href="/flower/">
        Blow my mind 🤯
      </Link>
      <Link class="todolist" href="/todolist/">
        TODO demo 📝
      </Link> */}
    </div>
  );
});

export const head: DocumentHead = {
  title: "Welcome to QR-Auther",
  meta: [
    {
      name: "description",
      content: "QR-Auther site description",
    },
  ],
};
