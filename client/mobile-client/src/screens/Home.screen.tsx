import React, { useState, useEffect } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Camera, CameraType } from "expo-camera";
import styles from "../../assets/styles";

export default function HomeScreen() {
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const [type, setType] = useState(CameraType.front);

  function toggleCameraType() {
    setType((current) =>
      current === CameraType.back ? CameraType.front : CameraType.back
    );
  }

  useEffect(() => {
    return () => {
      requestPermission();
    };
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.appHeader}>Welcome to QR-Auther</Text>
      <View style={styles.bgBox}>
        <Text style={{ color: "transparent" }}>Bg Box</Text>
      </View>
      <Camera style={styles.camera} type={type}>
        <Text style={styles.cornerBox}>Scan The QR</Text>
      </Camera>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={toggleCameraType}>
          <Icon name="sync-alt" size={30} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
}
