import React, { useCallback } from "react";
import { SafeAreaView } from "react-native";
import { useFonts } from "expo-font";
import HomeScreen from "./src/screens/Home.screen";
import * as SplashScreen from "expo-splash-screen";

SplashScreen.preventAutoHideAsync();

export default function App() {
  const [fontsLoaded] = useFonts({
    "Italiana": require("./assets/fonts/Italiana-Regular.ttf"),
  });

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }

  return (
    <SafeAreaView
      style={{ flex: 1, position: "relative" }}
      onLayout={() => onLayoutRootView()}
    >
      <HomeScreen />
    </SafeAreaView>
  );
}
