const fileURL = "/Users/sumonroy/Desktop/side-projects/qr-generator/server/src/data/PERSON_DATA.json";
const fs = require("fs");

const validate = async (user) => {
  try {    
    const data = await fs.promises.readFile(fileURL);    
    let foundUser = JSON.parse(data).find(
      (person) =>
        person.email === user.email && person.password === user.password
    );
    if (foundUser!==null) return foundUser;    
    return {};
  } catch (err) {
    return err;
  }
};

module.exports = validate;
