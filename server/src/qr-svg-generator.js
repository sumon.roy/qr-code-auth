const QRCode = require("qrcode-svg");

const qrsvg = (data) => {
  try {
    const svg = new QRCode(data).svg();
    return svg;
  } catch (err) {
    console.error(err);
    return err;
  }
};

module.exports = qrsvg;
