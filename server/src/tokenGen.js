const uuid4 = require("uuid").v4
const fs = require("fs").promises
const fileURL = "/Users/sumonroy/Desktop/side-projects/qr-generator/server/src/data/TOKEN_DATA.json"

const tokenGenerate= async (user)=>{
    try{
        let data = JSON.parse(await fs.readFile(fileURL));
        let existingUser = data.filter((item) => item.id === user.id);      
        let token;  
        if(existingUser.length !== 0){
            data = data.map(item => {
                if(item.id === user.id){
                    item.uuid = uuid4();
                    token = item;
                    return item;
                }
                return item;
            })
            await fs.writeFile(fileURL,JSON.stringify(data));
        }
        else{
            token = {id:user.id,email:user.email,uuid: uuid4()};        
            data = [...data, token];
            await fs.writeFile(fileURL,JSON.stringify(data));
        }        
        return token;
    }catch(err){
        return err
    }
}

module.exports=tokenGenerate
